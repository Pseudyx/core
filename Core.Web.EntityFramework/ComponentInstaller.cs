﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;


namespace Tribal.Core.Web
{
    using Framework.ORM.Infrastructure;
    using Framework.ORM.Repository;
    using Framework.ORM.Service;
    using Infrastructure;
    public class ComponentInstaller : IWindsorInstaller
    {
        private string _connectionString = "Core";

        public ComponentInstaller(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<EntityDbContext>()
                    .DependsOn(Dependency.OnValue<string>(_connectionString))
                    .LifestyleTransient());

            container.Register(Component.For<ApplicationUserRole>()
              .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserLogin>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserClaim>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationRole>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationWebUser>()
               .LifestylePerWebRequest());

            container.Register(Classes.FromThisAssembly()
                .BasedOn<EntityService>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));
        }
    }
}
