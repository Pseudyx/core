﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace Core.Web.Infrastructure
{
    
    using Framework.ORM.Infrastructure;
    using Framework.Interface;
    using Manager;
    public class ApplicationWebUser : ApplicationUser, IApplicationUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager userManager, string authenticationType)
        {
            var userIdentity = await userManager.CreateIdentityAsync(this, authenticationType);
            //add custom user claims here
            return userIdentity;
        }
    }
}
