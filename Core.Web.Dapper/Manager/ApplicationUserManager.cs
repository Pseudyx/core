﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Core.Web.Manager
{
    using Infrastructure;
    public class ApplicationUserManager : UserManager<ApplicationWebUser, Guid>
    {
        public ApplicationUserManager(IUserStore<ApplicationWebUser, Guid> store) : base(store) { }
    }
}
